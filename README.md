# Jira Boards on TV add-on

This add-on provides a sprint and project overview page that fits neatly on a TV.
Great for team standups - hundle around the TV, and jots your memory.

## Development
* The add-on is built using atlassian-connect-express, and deployed on micros with a postgres backing for client info.
* Contributions are welcome!
