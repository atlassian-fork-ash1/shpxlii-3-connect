#!/usr/bin/env bash

VERSION=0.0.2

DOCKER_HOST=docker.atl-paas.net
NAMESPACE=atlassian
SERVICE_NAME=jira-boards-on-tv
export DOCKER_IMAGE=${DOCKER_HOST}/${NAMESPACE}/${SERVICE_NAME}
export DOCKER_TAG=${VERSION}
