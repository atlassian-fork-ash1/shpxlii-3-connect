const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const BUILD_DIR = path.resolve(__dirname, 'dist');
const APP_DIR = path.resolve(__dirname, 'src');

const config = {
  entry: ['babel-polyfill', APP_DIR + '/index.jsx'],
  output: {
    path: BUILD_DIR,
    pathinfo: true,
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.jsx$|\.js$/,
        include: APP_DIR,
        loader: 'eslint-loader',
        options: {
          emitError: true,
          fix: true,
        }
      },
      {
        test: /\.jsx$|\.js$/,
        include: APP_DIR,
        use: ['babel-loader']
      },
      {
        test: /\.css$/,
        use: [{
          loader: 'style-loader'
        },
        {
          loader: 'css-loader',
          options: {
            modules: true,
            camelCase: true,
            sourceMap: true
          }
        }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([BUILD_DIR]),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      MOCK: process.env.MOCK,
    }),
  ]
};

if (process.env.NODE_ENV === 'production') {
  config.plugins.push(new UglifyJsPlugin());
}

module.exports = config;
