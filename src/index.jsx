import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import ProjectTVView from './components/ProjectTVView.jsx';
import SprintTVView from './components/SprintTVView.jsx';
import CastView from './components/CastView.jsx';

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/project-tv-view" component={ProjectTVView} />
    <Route path="/sprint-tv-view" component={SprintTVView} />
    <Route path="/cast" component={CastView} />
  </Router>
), document.getElementById('app-root'));
