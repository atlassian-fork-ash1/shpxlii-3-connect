const shipitJson = {
  'projectName': 'Expandonauts',
  'projectAvatarSrc': 'https://growth.jira-dev.com/secure/projectavatar?pid=12400&avatarId=12500',
  'teamMembers': [
    {
      'name': 'Max Puckeridge',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Herman Chow',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cbf63a7aed346c43512c0acd3de68afe?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcbf63a7aed346c43512c0acd3de68afe%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Lily Myers',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Bartosz Swierczynski',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Yves-Emmanuel Jutard',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Gautham Renganathan',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Huy Huu Nguyen',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b540722db0f981d9f40d90ce62ee5ce5?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb540722db0f981d9f40d90ce62ee5ce5%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Nick Doherty',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cdd085155f779c58c3c80077eebb85bd?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcdd085155f779c58c3c80077eebb85bd%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Andrew Sharman',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Nick Dupuy',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d5bb5a63ed3a1aee949e5e9157ff5c38?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd5bb5a63ed3a1aee949e5e9157ff5c38%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Milan Ilavsky',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/3945a2c3a5e3be69a96fe5133729ea89?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3945a2c3a5e3be69a96fe5133729ea89%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Aman Mohla',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/f0ff02f2719b118bf4b797641fd88915?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Ff0ff02f2719b118bf4b797641fd88915%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Jack Field',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/2a61f51c670503ea60cfc296f1e0b0de?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F2a61f51c670503ea60cfc296f1e0b0de%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    {
      'name': 'Leandro Balan',
      'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    }
  ],
  'epicCount': 9,
  'issueCount': 260,
  'legendItems': [
    {
      'label': 'Done',
      'color': '#36B37E'
    },
    {
      'label': 'Design',
      'color': '#00B8D9'
    },
    {
      'label': 'Backlog',
      'color': '#795548'
    },
    {
      'label': 'In Progress',
      'color': '#FFAB00'
    },
    {
      'label': 'Has Blocker',
      'color': '#FF5630'
    },
    {
      'label': 'Selected for Development',
      'color': '#F06292'
    },
    {
      'label': 'Kick-off/QA',
      'color': '#0065FF'
    }
  ],
  'overallProgressData': [
    {
      'id': '1',
      'collectionName': 'Expandonauts',
      'count': 3,
      'color': '#0065FF',
      'name': 'Kick-off/QA',
      'ticketsUpdated': [
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-317',
            'name': 'Fundles 1 - Feature Flag + configuration'
          },
          'updatedStatus': {
            'label': 'Kick-off/QA',
            'color': '#0065FF'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-321',
            'name': 'Confluence share UI analytics'
          },
          'updatedStatus': {
            'label': 'Kick-off/QA',
            'color': '#0065FF'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-339',
            'name': 'Add analytics to resend recovery link in AID'
          },
          'updatedStatus': {
            'label': 'Kick-off/QA',
            'color': '#0065FF'
          }
        }
      ]
    },
    {
      'id': '3',
      'collectionName': 'Expandonauts',
      'count': 11,
      'color': '#FFAB00',
      'name': 'In Progress',
      'ticketsUpdated': [
        {
          'assignee': {
            'name': 'Milan Ilavsky',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/3945a2c3a5e3be69a96fe5133729ea89?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3945a2c3a5e3be69a96fe5133729ea89%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-334',
            'name': 'Build JSD hook on create project to add users chosen by XFlow'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-329',
            'name': 'JSD-optimise project QA'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-330',
            'name': 'Fix IE11 styling issue'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-331',
            'name': 'Get better Analytics Coverage'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-298',
            'name': 'Allow targeting on initial bundle and primary intent from Jira Feature Flags'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-337',
            'name': 'Have the new variable injected into the Jira feature flag context'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'assignee': {
            'name': 'Jack Field',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/2a61f51c670503ea60cfc296f1e0b0de?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F2a61f51c670503ea60cfc296f1e0b0de%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-313',
            'name': 'Fundles 1 - Jira FE onboarding app-switcher'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'assignee': {
            'name': 'Herman Chow',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cbf63a7aed346c43512c0acd3de68afe?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcbf63a7aed346c43512c0acd3de68afe%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-314',
            'name': 'Fundles 1 - Jira FE "create" CTA now showcases page'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'assignee': {
            'name': 'Herman Chow',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cbf63a7aed346c43512c0acd3de68afe?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcbf63a7aed346c43512c0acd3de68afe%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-318',
            'name': "Fundles 1 - create a co-space if it's worth it"
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-300',
            'name': 'Parse Confluence URL in AID'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-275',
            'name': 'Basic tests for the TableTree'
          },
          'updatedStatus': {
            'label': 'In Progress',
            'color': '#FFAB00'
          }
        }
      ]
    },
    {
      'id': '10001',
      'collectionName': 'Expandonauts',
      'count': 217,
      'color': '#36B37E',
      'name': 'Done',
      'ticketsUpdated': [
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-87',
            'name': 'Dismissal common class for tracking users who have dismissed previously'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Herman Chow',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cbf63a7aed346c43512c0acd3de68afe?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcbf63a7aed346c43512c0acd3de68afe%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-62',
            'name': '^ Outline tasks for start trial flow (JSWJSD-13)'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-66',
            'name': 'Run SQLRunner on JSD+JSW for words in summary'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-12',
            'name': 'JIRA Software end users can request a trial of JIRA Service Desk from their admin'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-97',
            'name': 'Implement modal for request and start trial flows'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-98',
            'name': 'Ensure browser compatibility'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-99',
            'name': 'Analytics - Cross Sell Dialog (Request Trial) in Epic Experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-100',
            'name': 'Hook request trial up to endpoint'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-101',
            'name': 'Ignore - Not needed anymore'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-102',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-14',
            'name': 'Listening to copy/paste events to highlight JSD to users pasting content from email'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-89',
            'name': 'Paste event handlers work in Chrome and Firefox'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-90',
            'name': 'E-mail related content is detected in Summary, Description, Comments'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-110',
            'name': 'Treatment is applied after issue created on Backlog'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-106',
            'name': 'Contextual dialog displays on flag link click'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-94',
            'name': "Copy/paste doesn't hinder performance"
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-107',
            'name': "Treatment is muted when \"Don't show me again\" is clicked"
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-108',
            'name': 'Treatment displays a flag after non-inline edit/create'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-109',
            'name': 'Only one flag can be displayed at any given time'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-95',
            'name': 'Analytics - Copy & Paste Treatment in Epic Experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-121',
            'name': 'Should we handle copy/paste on Create Issue Page?'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-129',
            'name': 'Treatment is applied when when issue created on Agile Board'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-130',
            'name': 'Treatment is applied when issue created on Create Issue page'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-131',
            'name': 'Treatment is snoozed after it was applied'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-132',
            'name': 'Treatment is applied when comment added/edited on Agile Board'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-133',
            'name': 'Treatment is applied when comment added/edited on Issue Details Page'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-136',
            'name': 'Paste events are detected in Summary and Description'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-137',
            'name': 'Treatment is applied even when relevant content in summary was NOT pasted'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-138',
            'name': 'Handle ENTER/blur for inline edit fields '
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-144',
            'name': "Subsequent pastes don't invalidate previous ones"
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-37',
            'name': ' If a JIRA Software customer uses Support-specific language on an issue, we cross-sell JIRA Service Desk'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-147',
            'name': 'Treatment leads to a different modal if trial already requested '
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-148',
            'name': 'Treatment displays an inline dialog after inline edit of Description on Issue Page'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-153',
            'name': 'Treatment displays an inline dialog after adding a comment on Issue Page'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-149',
            'name': 'Treatment is applied after issue edited on Agile Board'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-152',
            'name': 'Treatment displays paste-related copy if there was a relevant paste'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-21',
            'name': 'Show JSD reports within JSW reports screen to highlight JSD as a better tool for certain reporting'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-88',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-91',
            'name': 'Touchpoint implementation'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-92',
            'name': 'Ensure browser compatibility'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-93',
            'name': 'Correct modal content: copy & image'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-96',
            'name': 'Analytics - Reports Treatment in Epic Experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-123',
            'name': 'Design Review'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Herman Chow',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cbf63a7aed346c43512c0acd3de68afe?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcbf63a7aed346c43512c0acd3de68afe%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-13',
            'name': 'JIRA Software admins starting a trial of JIRA Service Desk in product can bring their users with them'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-75',
            'name': 'Update xflow dialog component in GAB to support JSD'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-76',
            'name': 'pull in xflow start trial component for admins in Jira'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-77',
            'name': 'Update xflow user-search to support JSD'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-78',
            'name': 'Experiment payload to set project role on next Project creation'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-160',
            'name': 'Analytics - Cross Sell Dialog (Start Trial) in Epic Experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-177',
            'name': 'Analytics - Admin Cross sell events'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-25',
            'name': 'JIRA Software end users can create JIRA Service Desk projects from inside JIRA Software.'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-158',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-162',
            'name': 'Analytics - Create project in Epic Experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-22',
            'name': 'Suggest JIRA Service Desk to a JIRA Software user that changes the reporter on an issue'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-111',
            'name': 'Hook reporter change to treatment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-112',
            'name': 'Implement popup that appears on reporter change'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-113',
            'name': 'Allow user to disable touchpoint'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-114',
            'name': 'Analytics - Reporter Change Treatment in Epic Experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-115',
            'name': 'Modal copy and hero image'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-116',
            'name': 'Browser compatibility'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-117',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-122',
            'name': 'Design Review'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-26',
            'name': 'JIRA Software users can see Support on an issue'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-134',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-135',
            'name': 'Integrate with server storage do disable treatment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-139',
            'name': 'Dev'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-161',
            'name': 'Analytics - SLA Treatment in Epic Experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Huy Huu Nguyen',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b540722db0f981d9f40d90ce62ee5ce5?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb540722db0f981d9f40d90ce62ee5ce5%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-29',
            'name': 'JIRA Software users can get feedback on releases using JIRA Service Desk'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-163',
            'name': 'Analytics - Release Treatment in Epic Experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-85',
            'name': 'Import X-Flow experiments as variants 1 & 3'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-124',
            'name': 'Move across App Switcher - JIRA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-125',
            'name': 'Move across Discover Applications'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-126',
            'name': 'Move across App Switcher - Admin'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-127',
            'name': 'Move across Manage Applications'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-140',
            'name': 'Check browser compatibility'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-141',
            'name': 'Design Review'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-142',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-143',
            'name': 'Analytics - All the treatments in Optimise'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-73',
            'name': 'Cohort size + Factorial experiment research'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-86',
            'name': 'Consolidate analytic convert/trigger events across experiments'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Andrew Sharman',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-154',
            'name': 'QA Session'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-155',
            'name': 'User prompt disappearing on scroll and shows every time'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-156',
            'name': "\"new\" lozenge doesn't disappear after JSD activation"
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-157',
            'name': 'Inline dialog closes if you click on it'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-165',
            'name': 'QA: SLA Treatment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-166',
            'name': 'QA startTrial'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-43',
            'name': 'Add beacon to Jira issue create screens for pasting into comments/description'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-46',
            'name': '^ Check how long to complete'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-57',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Nick Doherty',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cdd085155f779c58c3c80077eebb85bd?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcdd085155f779c58c3c80077eebb85bd%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-38',
            'name': 'JIRA Software customers can see Support details on the issue'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-39',
            'name': 'Software teams in JIRA Software can see how to improve collaboration with IT teams using JIRA Service Desk'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-150',
            'name': 'Implement the page'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-151',
            'name': 'Implement the plumbing'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-159',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Andrew Sharman',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-167',
            'name': 'Create full event map for Lily'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-63',
            'name': '^ Get numbers for reporter change on issues'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-68',
            'name': 'implementation'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-69',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-79',
            'name': 'launch and monitor the experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-65',
            'name': '^ Define pasting conditions'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-71',
            'name': 'QA for email copy/paste'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-67',
            'name': 'Get numbers for page views for reports page in JSW'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Herman Chow',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cbf63a7aed346c43512c0acd3de68afe?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcbf63a7aed346c43512c0acd3de68afe%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-72',
            'name': '^ Document API requirements for request access flow'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-74',
            'name': 'Setup experiment for "EPIC EXPERIMENT"'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-183',
            'name': 'Running + Analysing "Epic Experiment"'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-6',
            'name': 'Skip all start-eval steps from WAC, instead go directly to instance with eval already initiated'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-42',
            'name': 'Confirm feasibility of all trial starts taking reduced flow'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-84',
            'name': 'Analyse results from WAC flow experiment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-60',
            'name': 'Remove confirm detailed popup code from GAB'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Andrew Sharman',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-49',
            'name': 'Optimizely experiment setup & enrolment'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Andrew Sharman',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-50',
            'name': 'Simplify WAC Angular form'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Herman Chow',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cbf63a7aed346c43512c0acd3de68afe?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcbf63a7aed346c43512c0acd3de68afe%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-182',
            'name': 'React Dev in production - hotfix to remove.'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-170',
            'name': 'Improve event quality on WAC to track drop-off'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-187',
            'name': 'Yves on-board to Home + Atlaskit code'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-188',
            'name': 'Max on-board to Home + Atlaskit code'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-178',
            'name': 'Building out request trial flow within Home'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-195',
            'name': 'Add Atlaskit/XFlow into Home'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-196',
            'name': 'Show XFlow Component when query param present'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-202',
            'name': 'For JSD/JSW/Core make sure the "Manage Users" step is skipped'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-207',
            'name': 'Make a feature flag to wrap XFlow being triggered'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-208',
            'name': '(optional) Get access to Product Fabric Feature flags'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-210',
            'name': 'Update Atlaskit/XFlow to not rely on meta tags for username'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-211',
            'name': 'Gracefully intercept Home Onboarding when users come to XFlow'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-212',
            'name': 'Analytics Events'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-213',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-215',
            'name': 'Create Feature Flag for Start Trial Flow in Home'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-223',
            'name': 'Fix Home CSS to be allow XFlow component to be used there'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-227',
            'name': 'Build out functionality to handle Confluence user who has requested trial'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-228',
            'name': 'Implement a better approach to checking whether provision is done for Jira and Confluenc'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-247',
            'name': 'Ensure Props get sent with XFlow events in home'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-248',
            'name': 'Bump Xflow version (6.12.7) across all consumers'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-253',
            'name': 'Make sure requested flag is set on a per site basis'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-193',
            'name': 'Modify X-Flow service to support emails for JSW and Jira Core'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-200',
            'name': 'Update service with logic for the two additional products'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-201',
            'name': 'Contact EP and request two new emails'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-191',
            'name': 'Start-trial confirm modal'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-197',
            'name': "Add Jira Core to Atlaskit's Xflow"
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-198',
            'name': 'Add Jira Software to Atlaskit XFlow'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-199',
            'name': 'Update Atlaskit XFlow to allow skipping the "Manage Users" step'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-205',
            'name': 'Refactor XFlow to allow more products to be added with less duplication'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-206',
            'name': 'Update JSD XFlow Provider to include request trial'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-194',
            'name': 'Create new email templates for request trial on JSW and Jira Core'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Andrew Sharman',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-192',
            'name': 'Start-trial request-access modal'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Andrew Sharman',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-209',
            'name': 'Integrate X-Flow component in to Home'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Andrew Sharman',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-168',
            'name': 'Implement new form layout on WAC'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Nick Dupuy',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d5bb5a63ed3a1aee949e5e9157ff5c38?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd5bb5a63ed3a1aee949e5e9157ff5c38%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-226',
            'name': 'Implement A/B tracking + cohort splitting on signup form'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-181',
            'name': 'Re-test WAC sign-up simplified form and roll-out to 100% using optimisely'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-189',
            'name': 'QA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-245',
            'name': 'Adding Cors rules to Stargate for the User Preferences Service'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-252',
            'name': 'Update GAB to include new improvements to XFlow Component'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Aman Mohla',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/f0ff02f2719b118bf4b797641fd88915?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Ff0ff02f2719b118bf4b797641fd88915%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-333',
            'name': 'Improve XFlow Component grant access'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-335',
            'name': 'Change references to Jira Software in Grant Access to be more general'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-336',
            'name': "Ensure 'everyone' option pulls users across all Jira products & Confluence"
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-186',
            'name': 'Create re-usable modal for all optimise touch-points'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Milan Ilavsky',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/3945a2c3a5e3be69a96fe5133729ea89?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3945a2c3a5e3be69a96fe5133729ea89%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-184',
            'name': 'Add Optimise touch-point to app switcher in Jira'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-273',
            'name': 'Ensure Analytics are present on introduced components'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-146',
            'name': 'Add Optimise touch-point to discover applications in GAB'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-271',
            'name': 'Ensure Analytics are present on introduced components'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-295',
            'name': "Get Nick's approval for matching design & generally quality"
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-260',
            'name': 'Add JSD & Confluence Optimise touch-point to app switcher in Admin'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Max Puckeridge',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/a2665e06a4d7f8bd1a01ef82a685a71f?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fa2665e06a4d7f8bd1a01ef82a685a71f%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-281',
            'name': 'Prepare Feature Flags for JSD Optimise'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Milan Ilavsky',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/3945a2c3a5e3be69a96fe5133729ea89?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3945a2c3a5e3be69a96fe5133729ea89%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-301',
            'name': 'Change wording about billing contact email from 3 days to 10 days before due date in Xflow'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-204',
            'name': 'Funnel & Opportunity pre-analysis'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Andrew Sharman',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-203',
            'name': 'Bundling tech investigation'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-264',
            'name': 'Fixing bundle evaluation analytics'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-283',
            'name': 'Snapshot experiment for appswitcher and product logo analytics'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-267',
            'name': 'Bundle evaluation optimisation (fundle phase 1)'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-293',
            'name': 'Snapshot experiment to answer questions about the home button and the app switcher'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-303',
            'name': 'Investigate space creation and link APIs'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-268',
            'name': 'Fundle funnel blitz'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-282',
            'name': 'Compare Jira usage patterns by whether the instance ended up active in Confluence'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-285',
            'name': 'Identify cloudAdmin analytics'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-291',
            'name': 'Understand what happened to Confluence bundle starting product'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Yves-Emmanuel Jutard',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/79f42303b86dfa5330f24ce5934f6df3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F79f42303b86dfa5330f24ce5934f6df3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-277',
            'name': 'Hack a way to to a cross-product experiment for improving product bundling'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-280',
            'name': 'Pass the cohort to the cloud provisioner'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-284',
            'name': 'Investigate bundle and eval context in Confluence'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-306',
            'name': 'Have the information stored into the Catalog Service'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-309',
            'name': 'Modify project creation page'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Leandro Balan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-310',
            'name': 'Product - coordinate Fundles touchpoints with the relevant teams'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-311',
            'name': 'Fundles 1 - WAC touchpoints'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-222',
            'name': 'Fetching Data from Confluence'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-221',
            'name': 'Tree accessibility / ARIA'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-216',
            'name': 'Expandable Tree component'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-231',
            'name': 'Display a flat table'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-232',
            'name': 'Display nested rows'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-233',
            'name': 'Expand/collapse nodes'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-234',
            'name': 'Async data support'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-235',
            'name': 'Customizable column widths'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-236',
            'name': 'Handle long titles'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-237',
            'name': 'API: render prop'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-238',
            'name': 'API: props on single component facade'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-239',
            'name': 'API: conform to Atlaskit naming conventions'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-242',
            'name': 'Basic documentation'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-243',
            'name': 'Loading state'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-244',
            'name': 'Code cleanup / optimization'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-279',
            'name': 'Synchronous data support'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-225',
            'name': 'TreeTable analytics hooks'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-224',
            'name': 'PageTree initial setup'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-218',
            'name': 'Contributor avatars details'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-217',
            'name': 'Basic page tree data'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-241',
            'name': 'Handling error states for Confluence Page Tree'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Milan Ilavsky',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/3945a2c3a5e3be69a96fe5133729ea89?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3945a2c3a5e3be69a96fe5133729ea89%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-249',
            'name': 'Setup Repository for Confluence Page Tree and other Megatron components'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-257',
            'name': 'Integration with Project Pages App in Jira'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-265',
            'name': 'Flow annotations'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-266',
            'name': 'PageTree analytics'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-269',
            'name': 'TableTree design review updates'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-270',
            'name': 'setState() called on unmounted TableTree'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-274',
            'name': 'Advanced Stories for Confluence Page Tree'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-276',
            'name': 'Ellipsis is not displayed when long titles are cut off'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Gautham Renganathan',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/426e4499dbf2cdf05e324f9c3ad9d8fa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F426e4499dbf2cdf05e324f9c3ad9d8fa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-278',
            'name': 'Test cases for Confluence Page Tree'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-294',
            'name': 'Update design to conform to ADG3'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-256',
            'name': 'Extend basic analytics in AID'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-304',
            'name': 'Add missing analytics to AID button clicks'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-305',
            'name': 'Add missing analytics to AID page views'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-302',
            'name': 'Add analytics to JSS (self signup)'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-297',
            'name': 'Funnel Blitz of sharing/login flows'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-338',
            'name': 'Create Redash dashboard using events from existing Confluence invite emails'
          },
          'updatedStatus': {
            'label': 'Done',
            'color': '#36B37E'
          }
        }
      ]
    },
    {
      'id': '10103',
      'collectionName': 'Expandonauts',
      'count': 3,
      'color': '#00B8D9',
      'name': 'Design',
      'ticketsUpdated': [
        {
          'assignee': {
            'name': 'Nick Doherty',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cdd085155f779c58c3c80077eebb85bd?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcdd085155f779c58c3c80077eebb85bd%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-179',
            'name': 'Suggest Jira Service Desk to admins using the Issue Collector in Jira Software'
          },
          'updatedStatus': {
            'label': 'Design',
            'color': '#00B8D9'
          }
        },
        {
          'assignee': {
            'name': 'Nick Doherty',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cdd085155f779c58c3c80077eebb85bd?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcdd085155f779c58c3c80077eebb85bd%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-17',
            'name': 'Suggest Jira Service Desk to admins setting up email rules for Jira Software'
          },
          'updatedStatus': {
            'label': 'Design',
            'color': '#00B8D9'
          }
        },
        {
          'assignee': {
            'name': 'Nick Doherty',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cdd085155f779c58c3c80077eebb85bd?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcdd085155f779c58c3c80077eebb85bd%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-175',
            'name': 'Suggest JSD issue types when a JSW user raises a bug'
          },
          'updatedStatus': {
            'label': 'Design',
            'color': '#00B8D9'
          }
        }
      ]
    },
    {
      'id': '10118',
      'collectionName': 'Expandonauts',
      'count': 12,
      'color': '#795548',
      'name': 'Backlog',
      'ticketsUpdated': [
        {
          'ticket': {
            'id': 'KERBAL-164',
            'name': 'QA: Copy-paste (JSWJSD-14, JSWJSD-37)'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-272',
            'name': 'Ensure Analytics are present on introduced components'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-332',
            'name': 'Design Review with Nick'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-287',
            'name': 'Appswitcher: Track source params from appswitcher in Confluence'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-288',
            'name': 'Appswitcher: Track source params from appswitcher in Jira'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-289',
            'name': 'Appswitcher: Publish source params from appswitcher in Jira + Conf + Home + CloudAdmin'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-290',
            'name': 'Add tracking around avatar picker step'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-319',
            'name': 'Fundles 1 - Tech debt'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-250',
            'name': 'Setup unit testing'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-251',
            'name': 'Setup Storybook / dev env'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-340',
            'name': 'Handle existing src attributes from Confluence share email in AID'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-341',
            'name': 'Cleanup Confluence-specific code in AID'
          },
          'updatedStatus': {
            'label': 'Backlog',
            'color': '#795548'
          }
        }
      ]
    },
    {
      'id': '10400',
      'collectionName': 'Expandonauts',
      'count': 12,
      'color': '#F06292',
      'name': 'Selected for Development',
      'ticketsUpdated': [
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-316',
            'name': 'Fundles 1 - Jira FE / pages / remove the "NEW" lozenge for evaluators'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': {
            'name': 'Jack Field',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/2a61f51c670503ea60cfc296f1e0b0de?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F2a61f51c670503ea60cfc296f1e0b0de%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-315',
            'name': 'Fundles 1 - Jira FE reminder "need a page?"'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-312',
            'name': 'Fundles 1 - Jira early onboarding touchpoints'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-324',
            'name': 'Add cloudId to AID analytics'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': {
            'name': 'Lily Myers',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/d7b1e073c559a9d1466d5a97280e25ec?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fd7b1e073c559a9d1466d5a97280e25ec%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-255',
            'name': 'Discover analysis holes in common sharing/invite funnels in Confluence'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-322',
            'name': 'Push traceId from Confluence sharing UI'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-323',
            'name': 'Push traceId from emails'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-325',
            'name': 'Push traceId from URL bar when browsing Confluence'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': {
            'name': 'Bartosz Swierczynski',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/b38d9890057443e6b02a2c5e9515b906?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fb38d9890057443e6b02a2c5e9515b906%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-219',
            'name': 'PageTree performance'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-230',
            'name': 'WAC Flow - Post release debt'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': null,
          'ticket': {
            'id': 'KERBAL-299',
            'name': "Analytics clean up in Cloud Site Admin's pf-site-admin-ui"
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        },
        {
          'assignee': {
            'name': 'Herman Chow',
            'avatarSrc': 'https://d2b6kerldzk1uu.cloudfront.net/cbf63a7aed346c43512c0acd3de68afe?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fcbf63a7aed346c43512c0acd3de68afe%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
          },
          'ticket': {
            'id': 'KERBAL-326',
            'name': '[20%] Atlassian Shareables - Embed your Jira issues anywhere'
          },
          'updatedStatus': {
            'label': 'Selected for Development',
            'color': '#F06292'
          }
        }
      ]
    },
    {
      'id': '12000',
      'collectionName': 'Expandonauts',
      'count': 2,
      'color': '#FF5630',
      'name': 'Has Blocker',
      'ticketsUpdated': [
        {
          'ticket': {
            'id': 'KERBAL-286',
            'name': 'Make a consumable library to track across page navigation events'
          },
          'updatedStatus': {
            'label': 'Has Blocker',
            'color': '#FF5630'
          }
        },
        {
          'ticket': {
            'id': 'KERBAL-307',
            'name': 'Have a transformer providing the initial shopping cart content to Jira in time for LD injection'
          },
          'updatedStatus': {
            'label': 'Has Blocker',
            'color': '#FF5630'
          }
        }
      ]
    }
  ]
};

export default shipitJson;
