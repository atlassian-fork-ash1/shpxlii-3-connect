const project = {
  'expand': 'description,lead,issueTypes,url,projectKeys',
  'self': 'https://growth.jira-dev.com/rest/api/2/project/12400',
  'id': '12400',
  'key': 'KERBAL',
  'description': '',
  'lead': {
    'self': 'https://growth.jira-dev.com/rest/api/2/user?username=asharman',
    'key': 'asharman',
    'accountId': '655363:f4f351ba-3873-46e3-bd46-b5127e38ee66',
    'name': 'asharman',
    'avatarUrls': {
      '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
      '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
      '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue',
      '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ca07689c21512fdb9c122f7cb38ba244?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fca07689c21512fdb9c122f7cb38ba244%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue'
    },
    'displayName': 'Andrew Sharman',
    'active': true
  },
  'components': [{
    'self': 'https://growth.jira-dev.com/rest/api/2/component/16101',
    'id': '16101',
    'name': '20% time',
    'isAssigneeTypeValid': false
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/component/15303',
    'id': '15303',
    'name': 'Analysis',
    'description': 'Analysis',
    'isAssigneeTypeValid': false
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/component/15301',
    'id': '15301',
    'name': 'Dev',
    'description': 'Development',
    'isAssigneeTypeValid': false
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/component/15304',
    'id': '15304',
    'name': 'Experiment Running',
    'description': 'Experiment currently running',
    'isAssigneeTypeValid': false
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/component/15305',
    'id': '15305',
    'name': 'Prototype',
    'description': 'Prototype',
    'isAssigneeTypeValid': false
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/component/15302',
    'id': '15302',
    'name': 'Research',
    'description': 'Research',
    'isAssigneeTypeValid': false
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/component/15300',
    'id': '15300',
    'name': 'UX',
    'description': 'UX stage',
    'isAssigneeTypeValid': false
  }],
  'issueTypes': [{
    'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
    'id': '10002',
    'description': 'A task that needs to be done.',
    'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
    'name': 'Task',
    'subtask': false,
    'avatarId': 10318
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
    'id': '10003',
    'description': 'The sub-task of the issue',
    'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
    'name': 'Sub-task',
    'subtask': true,
    'avatarId': 10316
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
    'id': '10001',
    'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
    'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
    'name': 'Story',
    'subtask': false
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10004',
    'id': '10004',
    'description': 'A problem which impairs or prevents the functions of the product.',
    'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10303&avatarType=issuetype',
    'name': 'Bug',
    'subtask': false,
    'avatarId': 10303
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10000',
    'id': '10000',
    'description': 'Created by Jira Software - do not edit or delete. Issue type for a big user story that needs to be broken down.',
    'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/epic.svg',
    'name': 'Epic',
    'subtask': false
  }],
  'assigneeType': 'UNASSIGNED',
  'versions': [{
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14000',
    'id': '14000',
    'description': 'MK1 of the large JSW > JSD experiment',
    'name': 'Epic Experiment MK1',
    'archived': false,
    'released': true,
    'startDate': '2017-09-19',
    'releaseDate': '2017-10-20',
    'userStartDate': '19/Sep/17',
    'userReleaseDate': '20/Oct/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14100',
    'id': '14100',
    'description': 'R&D for MK1 of the large JSW > JSD experiment',
    'name': 'Epic Experiment - R&D',
    'archived': false,
    'released': true,
    'startDate': '2017-09-13',
    'releaseDate': '2017-09-27',
    'userStartDate': '13/Sep/17',
    'userReleaseDate': '27/Sep/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14200',
    'id': '14200',
    'name': 'Clean-out',
    'archived': false,
    'released': true,
    'releaseDate': '2017-09-19',
    'userReleaseDate': '19/Sep/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14400',
    'id': '14400',
    'name': 'WAC Flow Productionisation',
    'archived': false,
    'released': true,
    'startDate': '2017-10-18',
    'releaseDate': '2018-01-15',
    'userStartDate': '18/Oct/17',
    'userReleaseDate': '15/Jan/18',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14500',
    'id': '14500',
    'name': 'Optimise Productisation',
    'archived': false,
    'released': false,
    'startDate': '2017-10-30',
    'releaseDate': '2018-02-16',
    'overdue': true,
    'userStartDate': '30/Oct/17',
    'userReleaseDate': '16/Feb/18',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14600',
    'id': '14600',
    'name': 'Bundle Fundle R&D',
    'archived': false,
    'released': true,
    'releaseDate': '2017-12-01',
    'userReleaseDate': '01/Dec/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14700',
    'id': '14700',
    'name': 'MK1 cleanup',
    'archived': false,
    'released': true,
    'releaseDate': '2017-10-20',
    'userReleaseDate': '20/Oct/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14800',
    'id': '14800',
    'name': 'Experiment remnants',
    'archived': false,
    'released': true,
    'releaseDate': '2017-11-01',
    'userReleaseDate': '01/Nov/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14801',
    'id': '14801',
    'name': 'Hotfix - React devmode in prod',
    'archived': false,
    'released': true,
    'releaseDate': '2017-11-01',
    'userReleaseDate': '01/Nov/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/14802',
    'id': '14802',
    'name': 'Epic Experiment MK2',
    'archived': true,
    'released': false,
    'startDate': '2017-12-11',
    'userStartDate': '11/Dec/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15001',
    'id': '15001',
    'description': 'Create/Invite NUIE by end-user when DRS is enabled',
    'name': 'Create NUEI by DRS',
    'archived': false,
    'released': false,
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15002',
    'id': '15002',
    'name': 'Megatron Pagetree',
    'archived': false,
    'released': true,
    'startDate': '2017-12-01',
    'releaseDate': '2018-01-30',
    'userStartDate': '01/Dec/17',
    'userReleaseDate': '30/Jan/18',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15004',
    'id': '15004',
    'name': 'Atlaskit Treetable Component',
    'archived': false,
    'released': true,
    'releaseDate': '2018-02-20',
    'userReleaseDate': '20/Feb/18',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15006',
    'id': '15006',
    'name': 'WAC Experiment clean-up',
    'archived': false,
    'released': true,
    'releaseDate': '2017-11-20',
    'userReleaseDate': '20/Nov/17',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15007',
    'id': '15007',
    'name': 'WAC grant access for JSW, JSD, and Core',
    'archived': false,
    'released': false,
    'startDate': '2018-02-11',
    'releaseDate': '2018-03-02',
    'overdue': false,
    'userStartDate': '11/Feb/18',
    'userReleaseDate': '02/Mar/18',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15008',
    'id': '15008',
    'name': 'Fundle - Funnel blitz',
    'archived': false,
    'released': true,
    'startDate': '2018-01-01',
    'releaseDate': '2018-02-01',
    'userStartDate': '01/Jan/18',
    'userReleaseDate': '01/Feb/18',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15009',
    'id': '15009',
    'name': 'NUEI funnel blitz',
    'archived': false,
    'released': false,
    'startDate': '2018-01-29',
    'releaseDate': '2018-02-23',
    'overdue': true,
    'userStartDate': '29/Jan/18',
    'userReleaseDate': '23/Feb/18',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15010',
    'id': '15010',
    'name': 'Fundles 1 - On-boarding flow ',
    'archived': false,
    'released': false,
    'startDate': '2018-02-05',
    'releaseDate': '2018-03-08',
    'overdue': false,
    'userStartDate': '05/Feb/18',
    'userReleaseDate': '08/Mar/18',
    'projectId': 12400
  }, {
    'self': 'https://growth.jira-dev.com/rest/api/2/version/15011',
    'id': '15011',
    'name': 'Fundles 2 - Pages onboarding flow',
    'archived': false,
    'released': false,
    'startDate': '2018-02-25',
    'releaseDate': '2018-03-21',
    'overdue': false,
    'userStartDate': '25/Feb/18',
    'userReleaseDate': '21/Mar/18',
    'projectId': 12400
  }],
  'name': 'Expandonauts',
  'roles': {
    'atlassian-addons-project-access': 'https://growth.jira-dev.com/rest/api/2/project/12400/role/10300',
    'Service Desk Team': 'https://growth.jira-dev.com/rest/api/2/project/12400/role/10102',
    'Developers': 'https://growth.jira-dev.com/rest/api/2/project/12400/role/10103',
    'Service Desk Customers': 'https://growth.jira-dev.com/rest/api/2/project/12400/role/10101',
    'Observers': 'https://growth.jira-dev.com/rest/api/2/project/12400/role/10200',
    'Administrators': 'https://growth.jira-dev.com/rest/api/2/project/12400/role/10002',
    'Tempo Project Managers': 'https://growth.jira-dev.com/rest/api/2/project/12400/role/10100'
  },
  'avatarUrls': {
    '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12400&avatarId=12500',
    '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12400&avatarId=12500',
    '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12400&avatarId=12500',
    '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12400&avatarId=12500'
  },
  'projectTypeKey': 'software',
  'simplified': false
};

export default project;
