const allSprints = {// eslint-disable-line no-unused-vars
  'maxResults': 50,
  'startAt': 0,
  'isLast': true,
  'values': [{
    'id': 112,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/112',
    'state': 'closed',
    'name': 'Megatron Mk1 - Sprint 1',
    'startDate': '2017-06-29T00:15:21.137Z',
    'endDate': '2017-07-14T00:15:00.000Z',
    'completeDate': '2017-07-17T00:21:02.724Z',
    'originBoardId': 62,
    'goal': 'Start Trial Works manually in GAB'
  }, {
    'id': 113,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/113',
    'state': 'closed',
    'name': 'Megatron Mk1 - Sprint 2',
    'startDate': '2017-07-17T00:56:25.357Z',
    'endDate': '2017-07-28T00:56:00.000Z',
    'completeDate': '2017-07-31T00:29:32.820Z',
    'originBoardId': 62,
    'goal': '* Implement Start Trial in GAB so that it can be A/B tested'
  }, {
    'id': 127,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/127',
    'state': 'closed',
    'name': 'Megatron Mk1 - Sprint 3',
    'startDate': '2017-07-31T00:58:11.554Z',
    'endDate': '2017-08-11T00:58:00.000Z',
    'completeDate': '2017-08-14T00:26:07.644Z',
    'originBoardId': 62,
    'goal': '- Have "Start Trial" live on a test prod instance and ready to go into A/B testing (pending design QA)\n- (works in GAB, also sending to StreamHub)'
  }, {
    'id': 130,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/130',
    'state': 'closed',
    'name': 'Megatron Mk1 - Sprint 4',
    'startDate': '2017-08-15T06:10:38.986Z',
    'endDate': '2017-08-25T06:00:00.000Z',
    'completeDate': '2017-08-28T00:44:00.391Z',
    'originBoardId': 62,
    'goal': 'Deploy StartTrial in GAB and JIRA'
  }, {
    'id': 131,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/131',
    'state': 'closed',
    'name': 'Megatron Mk1 - Sprint 5',
    'startDate': '2017-08-28T01:11:37.070Z',
    'endDate': '2017-09-01T01:01:00.000Z',
    'completeDate': '2017-09-11T00:52:57.053Z',
    'originBoardId': 62,
    'goal': 'GAB and Appswitcher are deployed into production - We have an understanding of how we will deliver the opt out flag'
  }, {
    'id': 141,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/141',
    'state': 'closed',
    'name': 'Megatron Mk1 - Ribs - 6',
    'startDate': '2017-09-11T01:08:57.239Z',
    'endDate': '2017-09-27T00:58:00.000Z',
    'completeDate': '2017-10-09T06:47:22.508Z',
    'originBoardId': 62,
    'goal': 'Non-Admin Request Trial is Deployed\n\nMegatron Mk1 is COMPLETE'
  }, {
    'id': 145,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/145',
    'state': 'closed',
    'name': 'Megatron - Appletizer - 7',
    'startDate': '2017-10-09T06:56:23.302Z',
    'endDate': '2017-10-20T06:46:00.000Z',
    'completeDate': '2017-10-29T23:38:01.041Z',
    'originBoardId': 62,
    'goal': "- stub for Software Team Pages is in production (behind feature flag)\n- E2E environment setup on everybody's machine\n- derisking several unknowns around detecting Confluence activation, linking projects/spaces, permissions for creating spaces."
  }, {
    'id': 146,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/146',
    'state': 'closed',
    'name': 'Megatron - Baileys - 8',
    'startDate': '2017-10-30T00:32:23.067Z',
    'endDate': '2017-11-10T00:22:00.000Z',
    'completeDate': '2017-11-12T22:43:01.107Z',
    'originBoardId': 62,
    'goal': 'Deployed a start trial / request trial flow for Pages with new xflow component designs.\n\nWe have a working solution for using the xflow component & react in Backlog view.'
  }, {
    'id': 150,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/150',
    'state': 'closed',
    'name': 'Megatron - Cointreau - 9',
    'startDate': '2017-11-12T23:26:39.940Z',
    'endDate': '2017-11-23T23:16:00.000Z',
    'completeDate': '2017-11-27T07:02:18.824Z',
    'originBoardId': 62,
    'goal': 'Backlog view is working and complete in dev except for hiding due to opt-out / connie detection.\n\nIn Pages we can link spaces and all context-specific copy is being populated.'
  }, {
    'id': 153,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/153',
    'state': 'closed',
    'name': 'Megatron - Dr Pepper - 10',
    'startDate': '2017-11-27T07:25:52.106Z',
    'endDate': '2017-12-15T07:15:00.000Z',
    'completeDate': '2017-12-17T23:26:04.942Z',
    'originBoardId': 62,
    'goal': 'End to end software pages is demoable (not shipable); backlog view is shippable'
  }, {
    'id': 158,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/158',
    'state': 'closed',
    'name': 'Megatron - Espresso - 11',
    'startDate': '2017-12-18T00:53:56.445Z',
    'endDate': '2017-12-29T00:43:00.000Z',
    'completeDate': '2018-01-02T00:10:09.515Z',
    'originBoardId': 62,
    'goal': ''
  }, {
    'id': 160,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/160',
    'state': 'closed',
    'name': 'Megatron - Frappé - 12',
    'startDate': '2018-01-02T03:30:23.341Z',
    'endDate': '2018-01-13T03:20:00.000Z',
    'completeDate': '2018-01-15T00:37:29.414Z',
    'originBoardId': 62,
    'goal': "Get everything ready so we're able to deploy on the Jira Chopper session on the 15th Jan for Project Pages."
  }, {
    'id': 161,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/161',
    'state': 'closed',
    'name': 'Megatron - Goon - 13',
    'startDate': '2018-01-15T05:20:31.919Z',
    'endDate': '2018-01-25T05:10:00.000Z',
    'completeDate': '2018-01-30T23:42:58.480Z',
    'originBoardId': 62,
    'goal': 'Feature complete with New lozenge, Meatball menu and Opt-Out, without support for IE/Safari.'
  }, {
    'id': 166,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
    'state': 'closed',
    'name': 'Megatron & EUE - HotChoc - 14',
    'startDate': '2018-01-31T05:22:30.513Z',
    'endDate': '2018-02-13T06:00:00.000Z',
    'completeDate': '2018-02-15T00:26:39.898Z',
    'originBoardId': 62,
    'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
  }, {
    'id': 167,
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
    'state': 'active',
    'name': 'Megatron & EUE-Irish Coffee-15',
    'startDate': '2018-02-15T04:55:56.833Z',
    'endDate': '2018-02-23T06:00:00.000Z',
    'originBoardId': 79,
    'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
  }]
};

export default allSprints;
