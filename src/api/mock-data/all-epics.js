const allEpics = { // eslint-disable-line no-unused-vars
  'maxResults': 50,
  'startAt': 0,
  'isLast': true,
  'values': [{
    'id': 107421,
    'key': 'KERBAL-61',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/107421',
    'name': 'Epic Experiment',
    'summary': 'Epic Experiment (experimentation)',
    'color': { 'key': 'color_8' },
    'done': false
  }, {
    'id': 105431,
    'key': 'KERBAL-48',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/105431',
    'name': 'WAC flow "trial start" experiment',
    'summary': 'WAC Flow experimentation',
    'color': { 'key': 'color_13' },
    'done': false
  }, {
    'id': 109605,
    'key': 'KERBAL-120',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/109605',
    'name': 'WAC Flow ',
    'summary': 'WAC Flow productionisation',
    'color': { 'key': 'color_2' },
    'done': false
  }, {
    'id': 110304,
    'key': 'KERBAL-145',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/110304',
    'name': 'Optimise productisation',
    'summary': 'JSD Optimise productisation',
    'color': { 'key': 'color_9' },
    'done': false
  }, {
    'id': 111922,
    'key': 'KERBAL-180',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/111922',
    'name': 'Fundles',
    'summary': 'Product bundling',
    'color': { 'key': 'color_8' },
    'done': false
  }, {
    'id': 113208,
    'key': 'KERBAL-214',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/113208',
    'name': 'Megatron Pagetree component',
    'summary': 'Building out atlaskit component for Megatron pagetree',
    'color': { 'key': 'color_10' },
    'done': false
  }, {
    'id': 114022,
    'key': 'KERBAL-254',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114022',
    'name': 'NUIE Initiative',
    'summary': 'Experiments / Productionisation focused on NUIE through invite/sharing techniques',
    'color': { 'key': 'color_9' },
    'done': false
  }, {
    'id': 114091,
    'key': 'KERBAL-258',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114091',
    'name': '',
    'summary': 'XFlow Grant Access Improvements',
    'color': { 'key': 'color_1' },
    'done': true
  }, {
    'id': 114093,
    'key': 'KERBAL-259',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114093',
    'name': '',
    'summary': 'JSD Optimise Productionization',
    'color': { 'key': 'color_1' },
    'done': true
  }, {
    'id': 114437,
    'key': 'KERBAL-292',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114437',
    'name': 'Tech Debt',
    'summary': 'Technical debt caused by experiments or discovered by them',
    'color': { 'key': 'color_5' },
    'done': false
  }, {
    'id': 114687,
    'key': 'KERBAL-328',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114687',
    'name': 'Expandonauts 20% time',
    'summary': 'Expandonauts 20% time',
    'color': { 'key': 'color_8' },
    'done': false
  }]
};

export default allEpics;
