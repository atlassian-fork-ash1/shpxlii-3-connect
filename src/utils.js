/* global AP, MOCK */
export const getQueryParameter = (param) => {
  return (window.location.search.split(param + '=')[1] || '').split('&')[0];
};

export const getMetaValue = (key) => {
  const metas = document.getElementsByTagName('meta');
  for (let i=0; i<metas.length; i++) {
    if (metas[i].getAttribute('name') === key) {
      return metas[i].getAttribute('content');
    }
  }
  return '';
};

export const getHost = () => {
  if (MOCK) {
    return 'https://growth.jira-dev.com';
  } else if (typeof AP !== 'undefined') {
    return AP._hostOrigin;
  } else {
    // TODO this case is for chromecast, needs to be handled better
    return '/';
  }
};
