import React from 'react';
import styled from 'styled-components';

import RecentActivity from './RecentActivity.jsx';
import TeamOverview from './TeamOverview.jsx';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

class Sidebar extends React.PureComponent {
    static propTypes = {
      ...RecentActivity.propTypes,
      ...TeamOverview.propTypes,
    }

    render() {
      const { activityGroups, members } = this.props;

      return (
        <Container>
          <RecentActivity activityGroups={activityGroups}/>
          <TeamOverview members={members}/>
        </Container>
      );
    }
}

export default Sidebar;
