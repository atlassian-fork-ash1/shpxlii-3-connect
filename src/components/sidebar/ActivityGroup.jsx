import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import _ from 'lodash';

import TicketUpdate from '../common/TicketUpdate.jsx';
import TicketListModal from '../common/TicketListModal.jsx';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 16px;
`;

const Label = styled.div`
  font-size: 14px;
  color: #8993A4;
  margin-bottom: 8px;
`;

const UpdateWrapper = styled.div`
  margin-bottom: 3px;
`;

const MoreItems = styled.div`
  font-size: 14px;
  color: #8993A4;
  display: flex;
  justify-content: flex-end;
  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
`;

class ActivityGroup extends React.PureComponent {
  static propTypes = {
    label: PropTypes.string.isRequired,
    ticketsUpdated: PropTypes.arrayOf(PropTypes.shape(TicketUpdate.propTypes)).isRequired,
    ticketsToShowInFull: PropTypes.number.isRequired,
  };

  state = { isModalOpen: false }
  openModal = () => this.setState({ isModalOpen:true });
  closeModal = () => this.setState({ isModalOpen:false });

  render() {
    const { isModalOpen } = this.state;
    const { label, ticketsUpdated, ticketsToShowInFull } = this.props;

    const topTickets = _.take(ticketsUpdated, ticketsToShowInFull);
    const remainder = ticketsUpdated.length - ticketsToShowInFull;

    return (
      <Container>
        <Label>{label}</Label>
        {
          topTickets.map((ticket, idx) =>
            <UpdateWrapper key={idx}>
              <TicketUpdate {...ticket}/>
            </UpdateWrapper>
          )
        }
        {
          remainder > 0 ? <MoreItems onClick={this.openModal}>+{remainder} more</MoreItems> : null
        }
        {
          remainder > 0 ?
            <TicketListModal
              isOpen={isModalOpen}
              onClose = {this.closeModal}
              title={label}
              ticketsUpdated={ticketsUpdated} /> : null
        }

      </Container>
    );
  }
}

export default ActivityGroup;
