import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import ActivityGroup from './ActivityGroup.jsx';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  font-size: 24px;
  font-weight: 500;
  color: #000000;
  margin-bottom: 17px;
`;

class RecentActivity extends React.PureComponent {
    static propTypes = {
      activityGroups: PropTypes.arrayOf(PropTypes.shape(ActivityGroup.propTypes)).isRequired
    }

    render() {
      const { activityGroups } = this.props;

      return (
        <Container>
          <Header>Recent activity</Header>
          {
            activityGroups.map((group, idx) =>
              <ActivityGroup {...group} key={idx}/>
            )
          }
        </Container>
      );
    }
}

export default RecentActivity;
