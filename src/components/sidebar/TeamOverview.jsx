import _ from 'lodash';
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { AvatarGroup } from '@atlaskit/avatar';

import TicketListModal from '../common/TicketListModal.jsx';
import TicketUpdate from '../common/TicketUpdate.jsx';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  font-size: 24px;
  font-weight: 500;
  color: #000000;
`;

const Subtitle = styled.div`
  color: #5E6C84;
  font-size: 14px;
  margin-bottom: 17px;
`;


const ActiveCount = styled.div`
  align-items: center;
  background-color: #2F80ED;
  color: white;
  display: flex;
  font-size: 0.85em;
  font-weight: 500;
  height: 100%;
  justify-content: center;
  text-align: center;
  width: 100%;
`;

class TeamOverview extends React.PureComponent {
  static propTypes = {
    members: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      avatarSrc: PropTypes.string.isRequired,
      activeCount: PropTypes.number.isRequired,
      ticketsUpdated: PropTypes.arrayOf(PropTypes.shape(TicketUpdate.propTypes)).isRequired,
    })).isRequired,
  }

  state = { isModalOpen: false, selectedItem: null }

  closeModal = () => this.setState({ isModalOpen: false, selectedItem: null });

  onClickItem = (item) => {
    this.setState({
      selectedItem: item,
      isModalOpen: true,
    });
  }

  renderModal() {
    const { isModalOpen, selectedItem } = this.state;
    if (!isModalOpen) {
      return null;
    }

    const { name, ticketsUpdated } = selectedItem;

    const title = `Tickets updated by ${name}`;
    return <TicketListModal title={title}
      ticketsUpdated={ticketsUpdated}
      isOpen={isModalOpen}
      onClose={this.closeModal}
    />;
  }

  render() {
    const data = _(this.props.members)
      .orderBy('activeCount', 'desc')
      .map((member) => ({
        name: member.name,
        src: member.avatarSrc,
        presence: member.activeCount ? <ActiveCount>{member.activeCount}</ActiveCount> : null,
        onClick: () => this.onClickItem(member),
      }))
      .value();

    return (
      <Container>
        <Header>Team Overview</Header>
        <Subtitle>Number of active tickets assigned</Subtitle>
        <AvatarGroup
          appearance="grid"
          data={data}
          maxCount={15}
          size="large"
        />
        {this.renderModal()}
      </Container>
    );
  }
}

export default TeamOverview;
