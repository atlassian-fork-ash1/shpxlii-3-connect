import React from 'react';
import Page, { Grid, GridColumn } from '@atlaskit/page';
import styled from 'styled-components';
import '@atlaskit/css-reset';

import { getCastData } from '../data/project';
import { getQueryParameter, getMetaValue } from '../utils';

import ProjectHeader from './header/ProjectHeader.jsx';
import MainContent from './main-content/MainContent.jsx';
import Sidebar from './sidebar/Sidebar.jsx';
import Loading from './common/Loading.jsx';

const Wrapper = styled.div`
  box-sizing: border-box;
  padding: 20px;
  width: 100%;
`;

export default class CastView extends React.PureComponent {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    const load = () => this.load();
    const intervalId = setInterval(load, 3000);
    this.setState({
      boardId: getMetaValue('boardId'),
      token: getQueryParameter('token'),
      intervalId: intervalId
    }, () => this.load());
  }

  load() {
    const { boardId, token } = this.state;
    getCastData(boardId, token).then((board) => {
      this.setState({
        board: board,
        done: true,
      });
    });
  }

  render() {
    console.log('Refreshed...');
    if (!this.state.done) {
      return <Loading/>;
    }

    const { headerProps, mainContentProps, sidebarProps } = this.state.board;

    return (
      <Wrapper>
        <Page>
          <Grid layout="fluid">
            <GridColumn medium={12}>
              <ProjectHeader {...headerProps} />
            </GridColumn>
            {
              (headerProps.epicCount > 0 || headerProps.issueCount > 0) ?
                [
                  <GridColumn medium={8}>
                    <MainContent {...mainContentProps} />
                  </GridColumn>,
                  <GridColumn medium={4}>
                    <Sidebar {...sidebarProps} />
                  </GridColumn>
                ]
                : null
            }
          </Grid>
        </Page>
      </Wrapper>
    );
  }
}
