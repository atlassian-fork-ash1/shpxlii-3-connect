import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Tooltip from '@atlaskit/tooltip';

const Status = styled.div`
  color: ${(props) => props.color};
  border: 1px solid ${(props) => props.color};
  border-radius: 6px;
  text-transform: uppercase;
  text-align: center;
  margin-left: 8px;
  height: 24px;
  line-height: 24px;
  margin-top: 1px;
  width: 72px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  padding: 0 4px;
`;

class TicketStatus extends React.PureComponent {
    static propTypes = {
      color: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }

    render() {
      const { color, label } = this.props;

      return (
        <Tooltip content={label}>
          <Status color={color}>{label}</Status>
        </Tooltip>
      );
    }
};

export default TicketStatus;
