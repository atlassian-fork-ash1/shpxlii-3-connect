import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Avatar from '@atlaskit/avatar';

import TicketText from './TicketText.jsx';
import TicketStatus from './TicketStatus.jsx';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  height: 28px;
  line-height: 28px;
`;

class TicketUpdate extends React.PureComponent {
    static propTypes = {
      assignee: PropTypes.shape({
        name: PropTypes.string.isRequired,
        avatarSrc: PropTypes.string.isRequired,
      }),
      ticket: PropTypes.shape(TicketText.propTypes),
      updatedStatus: PropTypes.shape(TicketStatus.propTypes),
    }
    render() {
      const { assignee, ticket, updatedStatus } = this.props;

      return (
        <Container>
          {
            assignee ?
              <Avatar size="small" name={assignee.name} src={assignee.avatarSrc}/> :
              <Avatar size="small" name="Unassigned"/>
          }
          <TicketText {...ticket}/>
          <TicketStatus {...updatedStatus}/>
        </Container>
      );
    }
};

export default TicketUpdate;
