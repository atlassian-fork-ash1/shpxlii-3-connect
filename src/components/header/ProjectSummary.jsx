import React from 'react';
import styled from 'styled-components';

import ProjectName from './ProjectName.jsx';
import ProjectLogo from './ProjectLogo.jsx';
import TeamMembers from './TeamMembers.jsx';

const ProjectSummaryContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

class ProjectSummary extends React.PureComponent {
    static propTypes = {
      ...ProjectLogo.propTypes,
      ...ProjectName.propTypes,
      ...TeamMembers.propTypes,
    }

    render() {
      const { projectAvatarSrc, projectName, teamMembers } = this.props;

      return (
        <ProjectSummaryContainer>
          <ProjectLogo projectAvatarSrc={projectAvatarSrc}/>
          <ProjectName projectName={projectName}/>
          <TeamMembers teamMembers={teamMembers}/>
        </ProjectSummaryContainer>
      );
    }
}

export default ProjectSummary;
