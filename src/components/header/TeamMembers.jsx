import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { AvatarGroup } from '@atlaskit/avatar';

const TeamMembersContainer = styled.div`
  max-width: 270px;
  margin-left: 5px;
`;

class TeamMembers extends React.PureComponent {
    static propTypes = {
      teamMembers: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        avatarSrc: PropTypes.string.isRequired,
      })).isRequired,
    }

    render() {
      const data = this.props.teamMembers.map((member) => {
        return {
          name: member.name,
          src: member.avatarSrc,
          appearance: 'circle',
          size: 'medium',
          enableTooltip: true,
        };
      });

      return (
        <TeamMembersContainer>
          <AvatarGroup
            appearance="stack"
            data={data}
            size="medium"
          />
        </TeamMembersContainer>
      );
    }
}

export default TeamMembers;
