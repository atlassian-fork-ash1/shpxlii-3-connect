import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';

const Wrapper = styled.div`
    font-size: 18px;
    color: #8993A4;
    margin-left: 8px;
`;

class SprintEnds extends React.PureComponent {
    static propTypes = {
      sprintEndDate: PropTypes.string.isRequired,
    }

    render() {
      const end = moment(this.props.sprintEndDate).format('Do MMMM YYYY');

      return <Wrapper>Sprint ends on the {end}</Wrapper>;
    }
}

export default SprintEnds;
