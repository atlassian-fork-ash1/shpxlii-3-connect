import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { AvatarGroup } from '@atlaskit/avatar';

const EpicMembersContainer = styled.div`
  max-width: 270px;
`;

class EpicMembers extends React.PureComponent {
    static propTypes = {
      members: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        avatarSrc: PropTypes.string.isRequired,
      })).isRequired,
    }

    render() {
      if (!this.props.members) {
        return null;
      }

      const data = this.props.members.map((member) => {
        return {
          name: member.name,
          src: member.avatarSrc,
          appearance: 'circle',
          size: 'small',
          enableTooltip: true,
        };
      });

      return (
        <EpicMembersContainer>
          <AvatarGroup
            appearance="stack"
            data={data}
            size="small"
          />
        </EpicMembersContainer>
      );
    }
}

export default EpicMembers;