import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';

import TicketUpdate from '../common/TicketUpdate.jsx';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Update = styled.div`
  color: #8993A4;
  font-size: 14px;  
`;

class EpicLastUpdated extends React.PureComponent {
    static propTypes = {
      timestamp: PropTypes.number.isRequired,
      ticketData: PropTypes.shape(TicketUpdate.propTypes),
    }

    render() {
      const { timestamp, ticketData } = this.props;

      const timeUpdated = moment.unix(timestamp).fromNow();

      return (
        <Container>
          <Update>Last updated: {timeUpdated}</Update>
          <TicketUpdate {...ticketData}/>
        </Container>
      );
    }
};

export default EpicLastUpdated;
