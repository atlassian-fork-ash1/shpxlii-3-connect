import React from 'react';
import { Grid, GridColumn } from '@atlaskit/page';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import EpicCard from './EpicCard.jsx';

const Container = styled.div`
    display: flex;
   flex-direction: column;
`;

const Heading = styled.div`
  font-size: 24px;
  font-weight: 500;
  color: #000000;
  margin-bottom: 17px;
`;

class MainContent extends React.PureComponent {
    static propTypes = {
      epics: PropTypes.arrayOf(PropTypes.shape(EpicCard.propTypes)).isRequired,
    }

    render() {
      const { epics } = this.props;

      return (
        <Container>
          <Heading>Overview</Heading>
          <Grid layout="fluid">
            {
              epics.map((epic, idx) =>
                <GridColumn medium={4} key={idx}>
                  <EpicCard {...epic}/>
                </GridColumn>
              )
            }
          </Grid>
        </Container>
      );
    }
}

export default MainContent;
