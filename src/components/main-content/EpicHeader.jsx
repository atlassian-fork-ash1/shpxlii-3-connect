import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Tooltip from '@atlaskit/tooltip';

import EpicMembers from './EpicMembers.jsx';
import { getHost } from '../../utils';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const WrappedTooltip = styled(Tooltip)`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  flex: 1;
`;

const Heading = styled.a`
  font-size: 20px;
  color: ${(props) => props.hasBlocker ? '#FF5630' : '#172B4D'};
  &:hover {
    color: ${(props) => props.hasBlocker ? 'rgba(255, 86, 48, 0.8)' : 'rgba(22,43,77,0.8)'};
  }
`;

class EpicHeader extends React.PureComponent {
    static propTypes = {
      ...EpicMembers.props,
      name: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      hasBlocker: PropTypes.bool.isRequired,
    }

    render() {
      const { name, members, id, hasBlocker } = this.props;
      const href = `${getHost()}/browse/${id}`;

      return (
        <Container>
          <WrappedTooltip content={name}>
            <Heading href={href} target="_blank" hasBlocker={hasBlocker}>{name}</Heading>
          </WrappedTooltip>
          <EpicMembers members={members}/>
        </Container>
      );
    }
};

export default EpicHeader;
