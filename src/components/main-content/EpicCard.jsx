import React from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';

import EpicHeader from './EpicHeader.jsx';
import EpicProgress from './EpicProgress.jsx';
import EpicLastUpdated from './EpicLastUpdated.jsx';

const RedBorderMixin = css`
  border-style: solid;
  border-color: rgba(255, 86, 48, 0.5);
  background-color: rgba(255, 86, 48, 0.03);
  border-width: 2px;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  box-shadow: 0px 1px 2px rgba(9, 30, 66, 0.25);
  border-radius: 6px;
  padding: 16px;
  height: 200px;
  box-sizing: border-box;
  margin-bottom: 16px;
  ${props => props.hasBlocker ? RedBorderMixin : null}
`;

class EpicCard extends React.PureComponent {
    static propTypes = {
      ...EpicHeader.propTypes,
      lastUpdated: PropTypes.shape(EpicLastUpdated.PropTypes),
      hasBlocker: PropTypes.bool.isRequired,
    }

    render() {
      const { name, id, members, issueCount, newIssueCount, progressData, lastUpdated, hasBlocker } = this.props;
      return (
        <Container hasBlocker={hasBlocker}>
          <EpicHeader
            name={name}
            id={id}
            members={members}
            hasBlocker={hasBlocker}
          />
          <EpicProgress
            issueCount={issueCount}
            newIssueCount={newIssueCount}
            progressData={progressData}
          />
          {lastUpdated ?
            <EpicLastUpdated {...lastUpdated}/> :
            null
          }
        </Container>
      );
    }
}

export default EpicCard;
