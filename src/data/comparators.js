const prioritisedOrderComparator = (order) => {
  return (a, b) => {
    if (a === b) {
      return 0;
    }

    let index = 0;
    while (index < order.length) {
      const current = order[index];

      if (a === current && b !== current) {
        return -1;
      }

      if (a !== current && b === current) {
        return 1;
      }

      index++;
    }

    return -1;
  };
};

export const issueStatusComparator = (a, b) => {
  const aType = a.updatedStatus.label;
  const bType = b.updatedStatus.label;

  return prioritisedOrderComparator([
    'Has Blocker',
    'Blocked',
    'Done',
    'In Progress'
  ])(aType, bType);
};

export const activityGroupComparator = (a, b) => {
  const aType = a.label;
  const bType = b.label;

  return prioritisedOrderComparator([
    'Last 30 minutes',
    'Last 3 hours',
    'Last day',
    'Last 3 days',
    'Last week',
    'Last month',
    'Last 3 months',
    'Longer than 3 months',
  ])(aType, bType);
};
