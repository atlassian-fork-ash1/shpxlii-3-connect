import _ from 'lodash';

const COLORS = [
  '#00B8D9',
  '#795548',
  '#F06292',
  '#0065FF',
  '#FF8F73',
  '#5E6C84',
  '#C0B6F2',
  '#9C27B0',
  '#8BC34A',
];

const RESERVED_COLORS = {
  'Done': '#36B37E',
  'Blocked': '#FF5630',
  'Has Blocker': '#FF5630',
  'In Progress': '#FFAB00',
  'To Do': '#6554C0',
};

const colorAssignments = {};
let remainingColors = _.clone(COLORS);

const makeColor = (name) => {
  if (name in RESERVED_COLORS) {
    return RESERVED_COLORS[name];
  }

  const assignment = _.head(remainingColors, 1);
  remainingColors = _.tail(remainingColors);
  return assignment;
};

export const getColorForStatus = ({ id, name }) => {
  if (id in colorAssignments) {
    return colorAssignments[id];
  }

  const assigned = makeColor(name);
  colorAssignments[id] = assigned;
  return assigned;
};
